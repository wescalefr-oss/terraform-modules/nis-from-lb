variable "lb_arn" {
  type        = string
  description = "ARN of the Load Balancer to search for its Network Interfaces"
}

variable "additional_filters" {
  type = list(
    object({
      name   = string
      values = list(string)
    })
  )
  description = "Additional filters to apply to the search, for precision purpose, description filter will be overriden by the module"
  default     = []
}