This project is archived, activity is being migrated to https://gitlab.com/wescalefr-oss/terraform-modules/terraform-aws-nis-from-lb for renaming purpose.      
Sources are kept for active stacks which would reference it. Please update your paths.

# Get Network Interfaces IDS from Load Balancer
This module provides a way to retrieve the network interfaces IDS associated with a LB.    
It uses Terraform >= 0.12 for structure.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| additional\_filters | Additional filters to apply to the search, for precision purpose, description filter will be overriden by the module | <pre>list(<br>    object({<br>      name   = string<br>      values = list(string)<br>    })<br>  )<br></pre> | `[]` | no |
| lb\_arn | ARN of the Load Balancer to search for its Network Interfaces | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| ids | List of the network interfaces IDs matching the filters |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
